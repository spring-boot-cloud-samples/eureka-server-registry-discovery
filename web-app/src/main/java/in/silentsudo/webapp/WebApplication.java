package in.silentsudo.webapp;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Map;

@EnableDiscoveryClient
@SpringBootApplication
public class WebApplication {


    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

    @Bean
    @LoadBalanced
    RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @AllArgsConstructor
    @NoArgsConstructor
    static class Account {

        @Getter
        String name;

        @Getter
        Long number;
    }

    @Slf4j
    @Service
    static class WebAccountService {
        static final String ACCOUNTS_SERVICE_URL = "http://ACCOUNT-SERVICE";
        private final RestTemplate restTemplate;

        @Autowired
        WebAccountService(RestTemplate restTemplate) {
            this.restTemplate = restTemplate;
        }

        Account getAccountDetails(Long number) {
            log.info("Checking {}", ACCOUNTS_SERVICE_URL);
            Account account = restTemplate.getForObject(ACCOUNTS_SERVICE_URL + "/accounts/" + number, Account.class);
            if (account == null) {
                account = new Account();
                log.error("Account api returned no account for account number {}", number);
            }
            return account;
        }
    }

    @RestController
    static class WebAccountController {

        private final WebAccountService webAccountService;

        @Autowired
        WebAccountController(WebAccountService webAccountService) {
            this.webAccountService = webAccountService;
        }

        @GetMapping("/account-details/{accountNumber}")
        public Map<String, Object> getAccountDetails(@PathVariable("accountNumber") Long number) {
            final Map<String, Object> response = new HashMap<>(2);
            response.put("account_number", String.valueOf(number));
            response.put("account_details", this.webAccountService.getAccountDetails(number));
            return response;
        }

    }
}
