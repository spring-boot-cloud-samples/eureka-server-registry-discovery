package in.silentsudo.accountservice;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@EnableDiscoveryClient
@SpringBootApplication
public class AccountServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(AccountServerApplication.class, args);
    }

    @RestController
    static class AccountServer {

        @GetMapping(value = "/", produces = MediaType.APPLICATION_JSON_VALUE)
        public Map<String, String> get() {
            final Map<String, String> response = new HashMap<>(1);
            response.put("status", "up");
            return response;
        }

        @GetMapping(value = "/accounts/{number}", produces = MediaType.APPLICATION_JSON_VALUE)
        public Account getAccount(@PathVariable("number") Long number) {
            return new Account("Premium User Account #" + number, number);
        }
    }

    @AllArgsConstructor
    @NoArgsConstructor
    static class Account {

        @Getter
        String name;

        @Getter
        Long number;
    }
}
