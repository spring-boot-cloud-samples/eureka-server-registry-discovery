## Service Registry & Discovery

- `eureka-server` is the service registry server
- `account-service` is the account service server which registers itself to `eureka-server`
- `web-app` is the main client facing server service server which registers itself to `eureka-server` and utilizes 
`account-service` vie `ACCOUNT-SERVICE` discovery and call the api with respected params i.e micro-services communicate
by their alis name specified in `application.yml` or visible in eureka server listening @localhost:1111

## Reference


